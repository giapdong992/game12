var MainChannel = {
    Scanner: require('./src/scanner'),
    Camera: require('./src/camera')
};

module.exports = MainChannel;