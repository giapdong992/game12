var app = new Vue({

    el: '#app',

    data: {
        state: {
            // Tìm và cắt đi những phần đen bên ngoài ảnh.
            //isCutting = true => Đã xác định được vị trí phần cắt.
            //positionCutting:[x,y,dx,dy] là dimention của ảnh.
            isCutting: false,
            positionCutting: null,
        },
        //Mỗi item là 1 object từ formatObjectDimention chứa đựng thông tin các thành phần:
        //Vị trí 5 tướng, vị trí vàng, vị trí level,.....
        listAllItem: [],
        //Data image: Thông tin của image.
        originalDataImage: {
            width: 0,
            height: 0,
            name: 'Dataset for original image',
            detail: [],
        },
        lineDataImage: {
            count: 5,
            name: 'Data set for training line yellow',
            list: []
        }
    },

    mounted: function() {
        //mounted event of browser
        //Lấy data từ XSplit truyền vào video coi như luồng quay video.
        var constraints = { audio: false, video: true };

        navigator.mediaDevices.getUserMedia(constraints).then(function(mediaStream) {
            var video = document.getElementById('preview');
            video.srcObject = mediaStream;
            video.play();
        }).catch(function(err) {
            console.log(err);
        })

    },

    methods: {

        formatName: function(name) {

            return name || '(unknown)';

        },
        takea() {
            console.log("Take Screen Shot")
        }
    },

});
//--------------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//
//Code for button Show Render Screen and Show Render Line

const buttonShow1 = document.querySelector('#show-1')
const buttonShow2 = document.querySelector('#show-2')

buttonShow1.onclick = function() {
    console.log("LEngth: " + app.originalDataImage.detail.length)

    app.originalDataImage.detail.forEach(element => {
        var show = '';
        element.forEach(child => {
            show += child + '||';
        });
        console.log(show);
    });
    console.log('------------------------------------------')

}
buttonShow2.onclick = function() {
    console.log(app.lineDataImage.list);
    console.log('------------------------------------------')
}

//--------------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//
//Code for task Render full Screen

const buttonStartRender = document.querySelector('.button-start-render');
const canvas = document.querySelector('#app #main-container canvas');
const video = document.querySelector('#preview');
const binary = document.querySelector('.result');

buttonStartRender.onclick = renderFullScreen;

function renderFullScreen() {
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    canvas.getContext('2d').drawImage(video, 0, 0);
    var imgData = canvas.getContext('2d').getImageData(0, 0, video.videoHeight, video.videoWidth).data;

    app.originalDataImage.width = video.videoWidth;
    app.originalDataImage.height = video.videoHeight;
    app.originalDataImage.detail = encodeData(imgData, video.videoWidth, video.videoHeight);

    let dataUrl = canvas.toDataURL('img/png');
    var hrefElement = document.createElement('a');
    hrefElement.href = dataUrl;
    document.body.append(hrefElement);
    hrefElement.download = `ScreenShot$.png`;
    //hrefElement.click();
    hrefElement.remove();

    console.log(`Video: ${video.videoWidth} - ${video.videoHeight}`)
    console.log("-------------------------")

}

//--------------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//
//Code for task request line and render, save in browser

const buttonRenderLine = document.querySelector('#main-container #button-render-line')
window.onload = function() {
    buttonRenderLine.onclick = renderLine;
}

function renderLine() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            var result = xhttp.response.split('-');
            app.lineDataImage.list = [];
            result.forEach(element => {
                var count = app.lineDataImage.count;
                canvas.nodeValue = null;

                canvas.width = count;
                canvas.height = count;

                var image = document.createElement('img');
                image.width = image.height = count;
                image.src = 'data:image/png;base64,' + element;

                canvas.getContext('2d').drawImage(image, 0, 0);
                var imgData = canvas.getContext('2d').getImageData(0, 0, count, count);
                console.log(imgData.data)
                app.lineDataImage.list.push([...encodeData4line(imgData, count, count)]);


                image.remove();
            });

            console.log("Done render line")
            console.log(app.lineDataImage.list)
            console.log('-----------------------------------')

        }
    };
    xhttp.open("GET", "public/folder/lineTraining", true);
    xhttp.send();

}

CheckItem = function() {
    //return true =>Continue loop
    //return false => stop
    if (app.lineDataImage.list.length == 0) {
        console.log("Length of list: 0");
        return true;
    } else {
        var boolCheck = false;
        app.lineDataImage.list.forEach(element => {
            if (element.length == 0) {
                boolCheck = true;
                console.log("Item length: 0");
                console.log(element)
            }
        });

        return boolCheck;
    }

}

function encodeData(imgData, width, height) {
    //parameter:
    //imgData: array all element of RGB.
    //imgData is raw array RGB from video

    var rawResult = [];
    var length = imgData.length;
    //first for loop
    //take rawdata to list pixel
    for (var i = 0; i < length; i += 4) {
        var temp1 = [imgData[i], imgData[i + 1], imgData[i + 2], imgData[i + 3]];
        rawResult.push([...temp1]);
    }
    var result = [];
    length = rawResult.length;
    //second for loop
    //from list pixel, take array with height element, each have width pixel.
    for (var i = 0; i < length; i += width) {
        var temp = rawResult.slice(i, i + width);
        if (!(temp[0][0] == 0 && temp[0][1] == 0 && temp[0][2] == 0 && temp[0][3] == 0))
            result.push([...temp]);
    }


    console.log("LEngth: " + result.length)

    result.forEach(element => {
        var show = '';
        element.forEach(child => {
            show += child + '||';
        });
        console.log(show);
    });


    return result;
}

function encodeData4line(imgData, width, height) {
    //parameter:
    //imgData: array all element of RGB.
    //imgData is raw RGB data for image

    var rawResult = [];
    var length = imgData.data.length;
    //first for loop
    //take rawdata to list pixel
    for (var i = 0; i < length; i += 4) {
        var temp1 = [imgData.data[i], imgData.data[i + 1], imgData.data[i + 2], imgData.data[i + 3]];
        rawResult.push([...temp1]);
    }
    var result = [];
    length = rawResult.length;
    //second for loop
    //from list pixel, take array with height element, each have width pixel.
    for (var i = 0; i < length; i += width) {
        var temp = rawResult.slice(i, i + width);
        if (!(temp[0][0] == 0 && temp[0][1] == 0 && temp[0][2] == 0 && temp[0][3] == 0))
            result.push([...temp]);
    }

    /*
    console.log("LEngth: " + result.length)

    result.forEach(element => {
        var show = '';
        element.forEach(child => {
            show += child + '||';
        });
        console.log(show);
    });
    */
    return result;
}
//--------------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//

//button-render-tier
//Onclick for render all tier
const buttonRenderTier = document.querySelector('#button-render-tier');
buttonRenderTier.onclick = function() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {

            var result = xhttp.responseText.split('-');
            result.pop();
            result.forEach(element => {
                console.log(element)
            });

            console.log("Done render Tier")
            console.log('-----------------------------------')

        }
    };
    xhttp.open("GET", "public/folder/Tier", true);
    xhttp.send();
}



function changeVidQualityFunction() {

    $chosenVidQuality = 'HD 1080';
    $trulycompletevideolink = document.getElementById("video1").src;
    $step1 = document.getElementById("video1").src.split("_q_");
    //COMMENT: step1[0] is the url from start and including the first part of the filename (not the "_q_"-part and the format)
    $upToVidName = $step1[0];
    //COMMENT: step1[1] is the resolution and format, e.g. 320.ogg
    $step2 = $step1[1].split(".");
    //COMMENT: step2[0] is the resoltion e.g. 720 ,step2[1] is the format (without the dot in front of the format type) e.g. ogg
    $vidresolution = $step2[0];
    $vidformat = $step2[1];

    $vidresolution = $chosenVidQuality;
    $result = $upToVidName + "_q_" + $vidresolution + "." + $vidformat;
    $('#video1').attr('src', $result);
    $('#video1V').attr('data-caption', $vidresolution + " OGG");
    $('#video1V').load();

    window.alert("video1 attr src:" + document.getElementById("video1").src); //shows updated URL

}
//--------------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//

//Button for video stream
//Link tham khao: https://developer.mozilla.org/en-US/docs/Web/API/Screen_Capture_API/Using_Screen_Capture

const startCaptures = document.querySelector('#start-capture');
const stopCaptures = document.querySelector('#stop-capture');
const takeScreenShot = document.querySelector('#take-screen-shot');

takeScreenShot.onclick = function() {
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;

    canvas.getContext('2d').drawImage(video, 0, 0);

    //dataUrl is base64 for png image.
    let dataUrl = canvas.toDataURL('img/png');
    img.src = dataUrl;
    var imgData = canvas.getContext('2d').getImageData(0, 0, 1080, 1920).data;

    var hrefElement = document.createElement('a');
    hrefElement.href = dataUrl;
    document.body.append(hrefElement);
    hrefElement.download = `ScreenShot$.png`;
    hrefElement.click();
    hrefElement.remove();
}

startCaptures.addEventListener("click", function(evt) {
    startCapture();
}, false);

stopCaptures.addEventListener("click", function(evt) {
    stopCapture();
}, false);

var displayMediaOptions = {
    video: {
        cursor: "never"
    },
    audio: false
};

async function startCapture() {
    try {
        video.srcObject = await navigator.mediaDevices.getDisplayMedia(displayMediaOptions);
        dumpOptionsInfo();
    } catch (err) {
        console.error("Error: " + err);
    }
}

function stopCapture(evt) {
    let tracks = video.srcObject.getTracks();

    tracks.forEach(track => track.stop());
    video.srcObject = null;
}

function dumpOptionsInfo() {
    const videoTrack = video.srcObject.getVideoTracks()[0];

    console.info("Track settings:");
    console.info(JSON.stringify(videoTrack.getSettings(), null, 2));
    console.info("Track constraints:");
    console.info(JSON.stringify(videoTrack.getConstraints(), null, 2));
}

//--------------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//

//Function support

function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}

function formatObjectDimention(x, y, dx, dy) {
    var result = {};
    result.Name = 'Name';
    result.dimention = [x, y, dx, dy];

    return result;
}