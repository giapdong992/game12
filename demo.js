var array1 = [102, 102, 51];
var array2 = [51, 51, 51];

function distance(v1, v2) {
    var d = Math.sqrt(
        (v1[0] - v2[0]) * (v1[0] - v2[0]) +
        (v1[1] - v2[1]) * (v1[1] - v2[1]) +
        (v1[2] - v2[2]) * (v1[2] - v2[2]))

    d = d / (255 * Math.sqrt(3));
    return d;
}
var array3 = [51, 51, 0];
var array4 = [];
console.log("Distance for Xsplit demo: " + distance(array1, array2))
console.log("Distance for Real demo: " + distance(array3, array2))
console.log('---------------------------------------------------')

function distance2(v1, v2) {
    var x = v1[0] - v2[0]
    var y = v1[1] - v2[1]
    var z = v1[2] - v2[2]

    x = Math.abs(x);
    y = Math.abs(y);
    z = Math.abs(z);
    x = x / 255.0;
    y = y / 255.0
    z = z / 255.0

    var percent = (x + y + z) / 3.0;
    return percent;
}

console.log("Distance for Xsplit demo: " + distance2(array1, array2))
console.log("Distance for Real demo: " + distance2(array3, array2))